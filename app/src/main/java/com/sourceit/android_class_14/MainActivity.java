package com.sourceit.android_class_14;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    public interface OnActionGenerate {

        void action(String string);
    }

    OnActionGenerate onActionGenerate=new OnActionGenerate() {
        @Override
        public void action(String stringToast) {
            Toast.makeText(MainActivity.this, stringToast,Toast.LENGTH_LONG ).show();
        }
    };

    MyService myService;
    Boolean isBinded;

    ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            MyService.MyBinder binder = (MyService.MyBinder) service;
            myService = binder.getService();

            myService.setOnActionGenerate(onActionGenerate);
            isBinded = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(MainActivity.this,MyService.class);
        bindService(intent, conn, BIND_AUTO_CREATE);

    }

    @Override
    protected void onStop() {
        super.onStop();
        myService.setOnActionGenerate(null);
        unbindService(conn);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button startButton = (Button) findViewById(R.id.start_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Intent intent = new Intent(MainActivity.this, MyService.class);
              //  Intent intent = new Intent(MainActivity.this, MyService.class);
               // startService(intent);
                Toast.makeText (MainActivity.this,myService.generateString(),Toast.LENGTH_LONG).show();
            }
        });

        Button stopButton = (Button) findViewById(R.id.stop_button);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Intent intent = new Intent(MainActivity.this, MyService.class);
               // stopService(intent);

                 myService.startDelay();

            }
        });


    }
}
