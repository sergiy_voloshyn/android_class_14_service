package com.sourceit.android_class_14;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import java.util.Random;

public class MyService extends Service {

    MainActivity.OnActionGenerate onActionGenerate;


    MyBinder myBinder = new MyBinder();

    public void setOnActionGenerate(MainActivity.OnActionGenerate onActionGenerate) {
        this.onActionGenerate = onActionGenerate;
    }

    class MyBinder extends Binder {

        MyService getService() {
            return MyService.this;
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, " Start service", Toast.LENGTH_LONG).show();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Toast.makeText(this, " Service destroyed", Toast.LENGTH_LONG).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        // throw new UnsupportedOperationException("Not yet implemented");

        return myBinder;
    }

    public String generateString() {
        return String.valueOf(new Random().nextInt());
    }

    public void startDelay()

    {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (onActionGenerate != null) {
                    onActionGenerate.action(generateString());
                }

            }
        }, 5000);

    }


}





